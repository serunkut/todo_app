FROM php:7.3-apache 
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo_mysql
RUN apt update -qq && \
	apt install -y vim locate git zip unzip && \
	updatedb
