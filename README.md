# TODO APP with docker wrapper

## docker-lamp

Docker with Apache, MySql 8.0, PhpMyAdmin and Php

You can use MySql 5.7 if you checkout to the tag `mysql5.7`

Docker-compose as an orchestrator. To run these containers:

```
docker-compose up -d
```

Open phpmyadmin at [http://localhost:8002](http://localhost:8002)

Open web browser to start the TODO APP at [http://localhost:8001/todo_app](http://localhost:8001/todo_app/)

Run mysql client:

- `docker-compose exec db mysql -u root -p`

MYSQL_DATABASE: task
MYSQL_USER: user
MYSQL_PASSWORD: test
MYSQL_ROOT_USER: root
MYSQL_ROOT_PASSWORD: test 
