<?php

require_once("config/autoload.php");
require_once("config/config.php");
//initialize logger
$GLOBALS["log"] = new \Log\Logger(LOG_LOCATION, LOG_LEVEL);
$controller = new \Controller\WebPageController(new \Controller\Actions(), new \Service\WebPageService(new \DAO\TaskDAO(new \DAO\DbConnector())));
$controller->initiate();


