<!--Design by https://www.sourcecodester.com/tutorials/php/12333/php-simple-do-list-app.html-->

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1"/>
</head>
<body>
<div class="col-md-3"></div>
<div class="col-md-6 well">
    <h3 class="text-primary">To Do List App</h3>
    <hr style="border-top:1px dotted #ccc;"/>
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <center>
            <form method="POST" class="form-inline" action="?action=add">
                <input type="text" class="form-control" name="task" required="required" pattern="^(?!\s*$).+"/>
                <button class="btn btn-primary form-control" name="add">Add Task</button>
            </form>
        </center>
    </div>
    <br/><br/><br/>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Task</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $this->createTodoList(); ?>
        </tbody>
    </table>
</div>
</body>
</html>