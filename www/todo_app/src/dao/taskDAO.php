<?php

namespace DAO;

/**
 * TaskDAO
 * @author erunkut
 */
class TaskDAO
{
    private $connection;

    public function __construct(\DAO\DatabaseInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * fetches and returns Task entries as a array of \Model\Task objects
     * @param string $sortingOrder
     * @return array of \Model\Task objects
     */
    public function getTasks($sortingOrder = "ASC")
    {
        $query = "SELECT * FROM `task` ORDER BY task_id " . $sortingOrder;
        $result = $this->connection->executeStatement($query, NULL, \PDO::FETCH_CLASS, "\Model\Task");
        return $result;
    }

    /**
     * @param $title
     */
    public function insertTask(string $title)
    {
        $query = "INSERT INTO task VALUES(NULL, ?, '')";
        $stmt = $this->connection->executeStatement($query, array($title));
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ': insert Task query result: ' . var_export($stmt, TRUE));

    }

    /**
     * @param int $task_id
     */
    public function updateTask(int $task_id)
    {
        $query = "UPDATE task SET status = 'Done' WHERE task_id = ?";
        $stmt = $this->connection->executeStatement($query, array($task_id));
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ': update Task query result: ' . var_export($stmt, TRUE));
    }

    /**
     * @param int $task_id
     */
    public function deleteTask(int $task_id)
    {
        $query = "DELETE FROM task WHERE task_id = ?";
        $stmt = $this->connection->executeStatement( $query, array($task_id));
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ': delete Task query result: ' . var_export($stmt, TRUE));

    }


}
