<?php

namespace View;

/**
 * MainView
 * @author erunkut
 */
class MainView
{
    private $taskList;

    public function __construct(array $taskList)
    {
        $this->taskList = $taskList;
    }

    /**
     * displays the HTML template
     *
     */
    public function displayPage()
    {
        require 'src/template/template.php';

    }

    /**
     * creates the HTML with Task values as a list
     */
    public function createTodoList()
    {
        $count = 1;
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' count: ' . var_export($this->taskList, true));
        foreach ($this->taskList as $fetch) {
            echo '<tr>
                  <td>' . $count++ . '</td>
                <td>' . $fetch->getTask() . '</td>
                <td>' . $fetch->getStatus() . '</td>
                <td colspan="2">';
            if ($fetch->getStatus() != "Done") {
                echo '<a href="?action=update&task_id=' . $fetch->getTaskId() . '" class="btn btn-success"><span class="glyphicon glyphicon-check"></span></a> | ';
            }
            echo '<a href="?action=delete&task_id=' . $fetch->getTaskId() . '" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>';

        }
    }


}