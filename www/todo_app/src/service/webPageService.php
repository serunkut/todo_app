<?php

namespace Service;

/**
 * WebPageService
 * @author erunkut
 */
class WebPageService
{
    private $taskDAO;

    public function __construct(\DAO\TaskDAO $dao)
    {
        $this->taskDAO = $dao;
    }

    /**
     * Converts the GET parameters into actions and returns them
     * consumes the \Controller\Actions class
     *
     * @param \Controller\Actions $actions
     * @return boolean
     **/
    public function checkGET(\Controller\Actions $actions)
    {
        $actionHtml = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
        if (!empty($actionHtml)) {
            $actions->setAction($actionHtml);
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return Tasks as arrray
     */
    public function getTasksList(): array
    {
        return $this->taskDAO->getTasks();
    }

    /**
     * central function for action handling
     * these actions are standardized (constants) and can be use from various interfaces like web, cli etc.
     *
     * @param \Controller\Actions $actions
     * @param \View\MainView $mainView
     * @throws \Exception
     */
    public function applyActions($action)
    {

        try {
            $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' action from user interface: ' . $action);
            switch ($action) {
                case 'add':
                    $title = filter_input(INPUT_POST, 'task', FILTER_SANITIZE_SPECIAL_CHARS);
                    $this->taskDAO->insertTask($title);
                    $this->redirect();
                    break;
                case 'update':
                    $task_id = filter_input(INPUT_GET, 'task_id', FILTER_SANITIZE_SPECIAL_CHARS);
                    $this->taskDAO->updateTask($task_id);
                    $this->redirect();
                    break;
                case 'delete':
                    $task_id = filter_input(INPUT_GET, 'task_id', FILTER_SANITIZE_SPECIAL_CHARS);
                    $this->taskDAO->deleteTask($task_id);
                    $this->redirect();
                    break;
                default:
                    $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ': GET action: ' . var_export($action, TRUE));
                    //throw new \Exception("not a valid action");
                    break;
            }
        } catch (\Throwable $t) {
            $this->webPageService->handleError($t);
        }
    }

    /**
     * Catches all unhandled errors that implement the throwable interface
     * @param type $t
     */
    public function handleError($t)
    {
        $GLOBALS["log"]->critical(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ': Error: ' . $t->getMessage() . "\nTrace: " . $t->getTraceAsString());
        die("Action stopped due to critical error! See log in " . LOG_LOCATION . "\n");
    }

    /**
     * redirecting to the given url even if the headers were loaded
     * @param string $url default
     */
    function redirect($url = "index.php")
    {
        if (!headers_sent()) {
            header('Location: ' . $url);
            exit;
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $url . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
            exit;
        }
    }

}
