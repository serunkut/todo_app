<?php

namespace Controller;

/**
 * WebPageController
 * @author erunkut
 */
class WebPageController {

    private $webPageService;
    private $controllerActions;

    public function __construct(\Controller\Actions $controllerActions, \Service\WebPageService $webPageService) {

        $this->controllerActions = $controllerActions;
        $this->webPageService = $webPageService;
    }

    /**
     * main controller method to handle requests
     * these actions are then applied 
     */
    public function initiate() {
        try {

            $results = $this->webPageService->getTasksList();

            $mainView =  new \View\MainView($results);
            $mainView->displayPage();

            if($this->webPageService->checkGET($this->controllerActions)) {
                $this->webPageService->applyActions($this->controllerActions->getAction());
            }

        } catch (\Throwable $t) {
            $this->webPageService->handleError($t);
        }
    }




}
