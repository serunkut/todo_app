<?php

namespace Controller;

/**
 * Actions
 * @author erunkut
 */
class Actions {

    const ACTION_NEW_ADD = "add";
    const ACTION_TOGGLE_UPDATE = "update";
    const ACTION_TOGGLE_DELETE = "delete";

    private $action;

    /**
     * Checks if actions are defined and returns them
     * @param array $actions
     * @throws \Exception
     */
    public function setAction(string $action) {
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' actions: ' . var_export($action, true));
        if (in_array($action, $this->getConstants())) {
            $this->action = $action;
        } else {
            throw new \Exception("action not defined");
        }
    }

    /**
     * Returns an array of \Controller\Actions
     * @return array of \Controller\Actions
     */
    public function getAction(): string {
        return $this->action;
    }

    /**
     * Returns an array of constants from the created reflection class
     * @return array
     */
    private function getConstants(): array {
        $refl = new \ReflectionClass('\Controller\Actions');
        return $refl->getConstants();
    }

}
