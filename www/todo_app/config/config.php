<?php

/**
 * config file for application constants
 * @author selim erunkut
 */

//database constants
DEFINE('DB_HOST', 'db');
DEFINE('DB_NAME', 'task');
DEFINE('DB_USER_NAME', 'root');
DEFINE('DB_PASS', 'test');
DEFINE('DB_CHARSET', 'utf8');
DEFINE("DSN", "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET);
DEFINE("PDO_OPTIONS", array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
));

//logger constants/configuration
DEFINE('LOG_LOCATION', dirname(__DIR__) . '/src/log');
DEFINE('LOG_LEVEL', Log\Psr\Log\LogLevel::DEBUG);